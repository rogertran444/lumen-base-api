<?php

namespace Psenna\LumenApi\Middleware;

use Closure;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Cookie;


class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->cookie(env('SESSAO_COOKIE_NAME', 'auth-user'));

        if (!$token)
        {
            return response()->json(["mensagem" => "Usuário não logado"], 401);
        }

        try {
            $user = JWT::decode($token, env('JWT_SECRET'), array('HS256'));

        } catch (\RuntimeException $e)  {
            $user = '';
        }

        if (!$token)
        {
            return response()->json(["mensagem" => "Login inválido"], 401)
                ->withCookie(new Cookie(env('SESSAO_COOKIE_NAME', 'auth-user'), '', 1 , '/', env('SESSAO_COKIE_HOST'), env('SESSAO_COKIE_SEGURO'), env('SESSAO_COKIE_HTTP')));
        }

        if (($user->expiracao ?? '') < Carbon::today()->format('Y-m-d')) {
            return response()->json(["mensagem" => "Sessão expirada"], 401)
                ->withCookie(new Cookie(env('SESSAO_COOKIE_NAME', 'auth-user'), '', 1 , '/', env('SESSAO_COKIE_HOST'), env('SESSAO_COKIE_SEGURO'), env('SESSAO_COKIE_HTTP')));
        }

        $request->user = $user;

        return $next($request);
    }
}