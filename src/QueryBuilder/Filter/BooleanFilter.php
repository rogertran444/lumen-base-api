<?php

namespace Psenna\LumenApi\QueryBuilder\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\Filters\Filter;

class BooleanFilter implements Filter
{
    protected $relationConstraints = [];

    public function __invoke(Builder $query, $value, string $property) : Builder
    {
        if ($this->isRelationProperty($property)) {
            return $this->withRelationConstraint($query, $value, $property);
        }

        if ($value == 'false' || $value == '') {
            return $query->where($property, '=', 0);
        }

        return $query->where($query->getQuery()->from . '.' .$property, '=', 1);
    }

    protected function isRelationProperty(string $property) : bool
    {
        return Str::contains($property, '.') && ! in_array($property, $this->relationConstraints);
    }

    protected function withRelationConstraint(Builder $query, $value, string $property) : Builder
    {
        [$relation, $property] = collect(explode('.', $property))
            ->pipe(function (Collection $parts) {
                return [
                    $parts->except(count($parts) - 1)->map('camel_case')->implode('.'),
                    $parts->last(),
                ];
            });

        return $query->whereHas($relation, function (Builder $query) use ($value, $relation, $property) {
            $this->relationConstraints[] = $property = $query->getModel()->getTable().'.'.$property;

            $this->__invoke($query, $value, $property);
        });
    }
}