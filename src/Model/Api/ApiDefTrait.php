<?php

namespace Psenna\LumenApi\Model\Api;

trait ApiDefTrait {
    public static function getAllowedFilters() {
        return [];
    }

    public static function getAllowedFields() {
        return [];
    }

    public static function getAllowedIncludes() {
        return [];
    }

    public static function getAllowedSort() {
        return [];
    }
}
